#!/bin/sh

# Installing i3 gaps
sudo add-apt-repository ppa:regolith-linux/unstable 
sudo apt update
sudo apt install i3-gaps

# fonts
sudo apt install fonts-noto*
sudo apt install fonts-source-code-pro-ttf
git clone https://gitlab.com/viocost/arch-fonts &&  cd arch-fonts && ./install.sh && cd ../


# arandr
sudo apt install arandr
sudo apt install brightnessctl

# Install random useful utils
sudo apt install tilix ranger dmenu rofi nitrogen picom polybar gucharmap htop tldr openssh openvpn openboard xfce4-appfinder curl sqlite3 rsync docker docker-compose -y

sudo systemctl enable sshd
sudo systemctl start sshd

sudo apt install chromium-browser -y
sudo apt install arc-theme arc-icon-theme lxappearence -y

# Devour
git clone https://github.com/salman-abedin/devour.git && cd devour && sudo make install

# zsh
sudo apt install zsh
chsh -s $(which zsh)
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
if [[ -f ~/.zshrc ]]; then
	rm ~/.zshrc
fi
ln -s $(pwd)/config/zsh/.zshrc $(readlink -f ~)

if [[ -f ~/.zprofile ]]; then
	rm ~/.zprofile
fi

ln -s $(pwd)/config/zsh/.zprofile $(readlink -f ~)

# nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash


# python 
sudo apt install python3-pip


# npm
sudo apt install npm

# bitwarden
sudo npm i -g @bitwarden/cli

./_bootstrap.sh


# emacs
sudo apt install emacs -y
git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d

if [[ ! -d ~/notes ]]; then
	mkdir ~/notes
fi
git clone https://github.com/viocost/emacs-doom ~/.doom.d





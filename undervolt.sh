#!/bin/bash



pip install undervolt


function create_undervolt_daemon(){
    echo "
[Unit]
Description=undervolt
After=suspend.target
After=hibernate.target
After=hybrid-sleep.target

[Service]
Type=oneshot
# If you have installed undervolt globally (via sudo pip install):
# ExecStart=/usr/local/bin/undervolt -v --core -110 --cache -110
# If you want to run from source:
ExecStart=/usr/local/bin/undervolt -v --core -110 --cache -110 --gpu -60 -t 85

[Install]
WantedBy=multi-user.target
WantedBy=suspend.target
WantedBy=hibernate.target
WantedBy=hybrid-sleep.target
" | sudo tee /etc/systemd/system/undervolt.service
    sudo systemctl enable undervolt
    sudo systemctl start undervolt
}

create_undervolt_daemon



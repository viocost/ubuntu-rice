#!/bin/bash

function yay_snapper_remove(){
    sudo snapper -c root create -c timeline,empty-pre-post -d "Removing package(s) $*" \
        --command "yay -R --noconfirm $*"
}


function pacman_snapper_remove(){
    sudo snapper -c root create -c timeline,empty-pre-post -d "Removing package(s) $*" \
        --command "sudo pacman -R  --noconfirm $*"
}

case $1 in
    pacman)
    echo removing ${@:2} with packman
    pacman_snapper_remove ${@:2}
    ;;
    yay)

    echo removing ${@:2} with yay
    yay_snapper_remove  ${@:2}
    ;;
    *)
    echo Invalid installer: $1
    exit 1
esac

#!/bin/bash

# i3 volume control script

newvol="pkill -RTMIN+10 i3blocks"
sample="paplay  ${HOME}/.config/i3/sound/pop1.wav"


case "$1" in
	"up") amixer -D pulse sset Master "$2"%+ ; $newvol ; $sample ;;
	"down") amixer -D pulse sset Master "$2"%- ; $newvol ; $sample ;;
	"mute") amixer -D pulse sset Master toggle  ; $newvol ; $sample ;;
esac

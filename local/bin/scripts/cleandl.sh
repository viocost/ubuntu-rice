#!/bin/bash
let TRESHOLD=3600*24*1
DL_DIR="/home/kostia/Downloads/"
CUR_DATE=$(date +%s)
TRASH_DIR=/home/kostia/trash
if [[ ! -d $TRASH_DIR ]]; then
    mkdir $TRASH_DIR
fi


for line in $(ls $DL_DIR); do
    DATE_STRING=$(ls -l --time-style=+"%Y-%m-%dT%H:%M:%S%:z" ${DL_DIR}/$line | awk '{print $6}')
    let DIFF=$CUR_DATE-$( date -d $DATE_STRING +%s )

    if (( $DIFF > $TRESHOLD  )); then
        mv $DL_DIR/$line $TRASH_DIR
    fi
done


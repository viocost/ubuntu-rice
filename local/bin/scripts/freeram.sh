#/bin/bash

echo Clearing RAM...
for i in 1 2 3; do
	sync; echo $i | sudo tee /proc/sys/vm/drop_caches > /dev/null
done

echo Done.

echo Clearing swap...
sudo swapoff -a && sudo swapon -a
echo Done.

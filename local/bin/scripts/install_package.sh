#!/bin/bash


function yay_snapper_install(){
    sudo snapper -c root create  -c timeline,empty-pre-post -d "Installing package $* " \
        --command "yay -Sy --noconfirm $*"
}


function pacman_snapper_install(){
    sudo snapper -c root create -c timeline,empty-pre-post -d "Installing package(s) $*" \
        --command "sudo pacman -Sy --noconfirm $*"
}


function is_snapper_ready(){
    echo "yes"
}

case $1 in
    pacman)
    echo installing ${@:2} with packman
    pacman_snapper_install ${@:2}
    ;;
    yay)

    echo installing ${@:2} with yay
    yay_snapper_install  ${@:2}
    ;;
    *)
    echo Invalid installer: $1
    exit 1
esac
